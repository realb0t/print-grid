Печать документа

Функционал печати документа должен поставляться 
как отдельный сервис печати.

При этом все праметры печати, такие как

- Размеры документа
- Разметка документа
- Макет документа
- Данные документа

должны приходить с клиента POST-запросом на сервер. 
И на этот запрос сервер должен выдавать статус печати.
Например, если печатается PDF документ то сервер выдает PDF.

**Данные документа** вынесенны на клиент во-первых: для того чтобы была возможность поставлять сервис-печати как API, во-вторых: для того чтобы документ перед печатью можно было просматривать на реальных данных.