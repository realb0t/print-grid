import MouseBackend from './dnd-mouse-backend/MouseBackend'

const createMouseBackend = (manager) => new MouseBackend(manager)

export default createMouseBackend
