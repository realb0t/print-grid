//
// Принтер для wkhtmltopdf
//
// На выходе PDF форматов A4 или Latter
//
// letter 796х1031
// A4 773х1095

'use strict';

import wkhtmltopdf from 'wkhtmltopdf';
import tmp from 'tmp';
import fs from 'fs';
import _ from 'underscore';

_.templateSettings = {
  interpolate : /\{\{(.+?)\}\}/g
};

var tmpHTML = `
<html lang="en-us">
  <head>
    <meta charset="utf-8" />
    <title>PrintGrid PDF Mock</title>
    <style type="text/css">
      @font-face {
        font-family: 'Lora';
        font-style: normal;
        font-weight: 400;
        src: url({{fontCDN}}/fonts/Lora.ttf) format('truetype');
      }

      @font-face {
        font-family: 'Marmelad';
        font-style: normal;
        font-weight: 400;
        src: url({{fontCDN}}/fonts/Marmelad.ttf) format('truetype');
      }

      body, html { margin: 0px; padding: 0px; }
      table { border: 0; cell-spacing: 0; }
      td { margin: 0; padding: 0; }
      svg { display: inline; margin: 0; padding: 0; }
      div.a4 { width: 773px; height: 1095px; }
      div.letter { width: 796px; height: 1031px; }
    </style>
  </head>
  <body>
    {{body}}
  </body>
</html>
`;

export default class {
  constructor(printerParams) {
    this.printerParams = printerParams;
    this.port = printerParams.port;
    this.svg = printerParams.svg;
    this.meta = printerParams.meta;
  }
  prepareData() {
    return { name: 'Казанцев Николай', crewName: 'Hasky' }
  }
  bodyHTML() {
    var dataTemp = _.template(this.svg);
    return '<div class="a4">' + dataTemp(this.prepareData()) +
      dataTemp(this.prepareData()) +
      dataTemp(this.prepareData()) +
      dataTemp(this.prepareData()) +
      dataTemp(this.prepareData()) +
      dataTemp(this.prepareData()) +
      dataTemp(this.prepareData()) +
      dataTemp(this.prepareData()) + '</div>';
  }
  print() {
    var tmpname = tmp.tmpNameSync({ template: '/tmp/tmp-wkhtmltopdf-XXXXXX' });
    return new Promise((resolve, reject) => {
      var templateParam = { 
        fontCDN: 'http://localhost:' + this.port, 
        body: this.bodyHTML() 
      };
      var resultHTML = _.template(tmpHTML)(templateParam);
      wkhtmltopdf(resultHTML, { 
        pageSize: this.meta.format, 
        orientation: this.meta.orientation,
        output: tmpname,
        marginBottom: 0, marginLeft: 0,
        marginTop: 0, marginRight: 0
      }, resolve);
    }).then(() => {
      var pdfBuffer = fs.readFileSync(tmpname);
      fs.unlinkSync(tmpname);
      return pdfBuffer;
    });
  }
}