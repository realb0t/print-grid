//
// Скрипт добавления SVG на страницу
// (для последующей печати в PDF)

var parent;
if (typeof window == 'undefined') {
  parent = {};
} else {
  parent = window;
}

parent.appendSvgHtml = function(svgHtml) {
  var div = document.createElement('div');
  div.innerHTML = svgHtml;
  document.body.appendChild(div.children[0]);
  return document.getElementsByTagName('svg')[0];
}

