//
// PDF-принтер
//
// Модуль получает в качестве параметров объект с полями
// port - порт на котором запущен основной процесс веб-сервера;
// svg  - HTML SVG-элемента переданный с клиента.
// Создает сессию с PhantomJS и добавляет SVG на страницу 
// (из скрипта printers/pdf/front.js)
// Потом после события onload с задержкой 100 мс (для
// рендаринга) делается скриншот изображения и 
// серилизуется в base64 для последующей передачи в PDF.
// Далее создает PDF-документ и подготавливает 
// его для передачи клиенту.
//
// Размер страницы 612х792

import PDFDocument from 'pdfkit';
import phantom from 'phantom';

export default class {
  constructor(printerParams) {
    this.printerParams = printerParams;
    this.page = null;
    this.phantomInstance = null;
    this.scale = printerParams.scale || 4;
    this.unscale = 1 / this.scale;
  }
  checkLoaded() {
    // Fresh meat
    // Надо проверить, т.к. рекурсивные промисы возможно зло
    return new Promise((done) => {
      setTimeout(() => {
        this.page.setting('isLoaded').then((result) => {
          if (!result) { this.checkLoaded(); } else { done(result); }
        })
      }, 0);
    });
  };
  createPage(ph) {
    console.log('1. Create phantom page');
    this.phantomInstance = ph;
    return ph.createPage();
  }
  setViewport(page) {
    console.log('2. Set viewportSize');
    this.page = page;
    return new Promise((resolve) => {
      page.property('viewportSize', {
        width: this.printerParams.width * this.scale, 
        height: this.printerParams.height * this.scale
      }).then(resolve);
    })
  }
  initializePage() {
    console.log('3. Set callbacks and open page');

    this.page.property('onConsoleMessage', /* PhantomJS context */function(message) {
      console.log(message);
    });

    this.page.property('onInitialized', /* PhantomJS context */function(scale) {
      this.evaluate(/* Browser context */function(scale) {
        document.addEventListener('DOMContentLoaded', function() {
          document.body.style.webkitTransform = "scale(" + scale + ")";
          document.body.style.webkitTransformOrigin = "0% 0%";
          document.body.style.width = 100/scale + "%";
        });
        window.onload = function() { callPhantom({ event: 'loaded' }); };
      }, scale);
    }, this.scale);

    this.page.property('onCallback', /* PhantomJS context */function(request) {
      if (request.event == 'loaded') { this.settings.isLoaded = true; }
    });

    return new Promise((resolve) => {
      var pageUrl = `http://localhost:${this.printerParams.port}/pdf_mock.html`;
      this.page.open(pageUrl).then(resolve);
    });
  }
  appendSVG() {
    console.log('4. Append SVG to DOM');

    return new Promise((resolve) => {
      this.page.evaluate(/* Browser context */function(svgHtml) {
        appendSvgHtml(svgHtml);
      }, this.printerParams.svg).then(resolve);
    });
  }
  screenshotPage() {
    console.log('5. Screenshoot page');

    return new Promise(resolve => {
      setTimeout(() => { // SVG font rendering delay
        this.page.renderBase64('PNG').then(dataUri => {
          resolve({ 
            width: this.printerParams.width, 
            height: this.printerParams.height, 
            dataUri: dataUri
          });
        });
      }, 300);
    });
  }
  makePDF(imageInfo) {
    console.log('6. Create pdf');
    return new Promise((resolve) => {
      let buffers = [];
      let dataUri = imageInfo.dataUri;
      let imageBuffer = new Buffer(dataUri.replace('data:image/png;base64,', ''), 'base64');
      let doc = new PDFDocument();
      doc.on('data', buffers.push.bind(buffers));
      doc.on('end', () => {
        resolve(Buffer.concat(buffers))
      });
      doc.image(imageBuffer, 0, 0, { 
        width: imageInfo.width, height: imageInfo.height, 
        scale: this.unscale });
      doc.end();
    });
  }
  finish(buffer) {
    console.log('7. Close page and PhantomJS Instance');
    this.closePhantom();
    return buffer;
  }
  closePhantom() {
    this.page.close();
    this.phantomInstance.exit();
  }
  print() {
    return phantom.create()
      .then(this.createPage.bind(this))
      .then(this.setViewport.bind(this))
      .then(this.initializePage.bind(this))
      .then(this.appendSVG.bind(this))
      .then(this.checkLoaded.bind(this))
      .then(this.screenshotPage.bind(this))
      .then(this.makePDF.bind(this))
      .then(this.finish.bind(this))
      .catch((err) => {
        this.closePhantom();
        throw err;
      });
  }
};