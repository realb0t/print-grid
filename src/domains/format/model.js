export default class FormatModel {
  constructor() {
    this.data = {
      format: 'A4',
      orientation: 'portrait',
      grid: '2x3'
    };
  }

  setData(data) {
    this.data = data;
    return this;
  }

  getData() {
    return this.data;
  }
}