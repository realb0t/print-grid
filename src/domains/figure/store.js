/*
 * Copyright (c) 2014, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 *
 * @flow
 */

'use strict';

import type { Action } from './FigureActions';
import Immutable from 'immutable';
import { ReduceStore } from 'flux/utils';
import Figure, { Rect, Image, Text } from './state';
import Dispatcher from './../dispatcher';

// Set up the store, If we didn't care about order we could just use MapStore
type State = Immutable.List<string, Figure>;

class FigureStore extends ReduceStore<string, Figure> {
  getInitialState(): State {
    let state = Immutable.List();
    state = state.push(new Rect({ left: 50, top: 50, width: 50, height: 50 }));
    state = state.push(new Image({ left: 80, top: 20, 
        width: (646 / 4), height: (541 / 4), 
        href: 'http://localhost:8000/images/apricot.jpg' }));
    state = state.push(new Text({ type: 'text', top: 80, 
        left: 80, content: '{{name}}' }));
    return state;
  }

  reduce (state: State, action: Action): State {
    switch (action.actionType) {
      case 'figure/add':
        return createFigure(state, action.figureType, action.params);

      case 'figure/move':
        return state.update(action.id, (figure) => {
          if (!figure) { return; }
          return figure.withMutations(ctx => {
            ctx.set('left', action.left).set('top', action.top);
          });
        });

      case 'figure/delete':
        return state.delete(action.id);

      case 'figure/select':
        state = state.map(v => v.withMutations(ctx => ctx.set('selected', false) ));
        return state.update(action.id, (figure) => {
          if (!figure) { return; }
          return figure.withMutations(ctx => ctx.set('selected', true))
        });

      default:
        return state;
    }
  }

  areAllComplete(): boolean {
    return this.getState().every(todo => todo.complete);
  }
}

function createFigure(state: State, figureType: string, params: ?Object): State {
  var newFigure = new Figure(params || {});
  return state.push(newFigure);
}

const instance = new FigureStore(Dispatcher);
export default instance;
