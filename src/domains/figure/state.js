/*
 * @flow
 */

'use strict';

import Immutable, { Record } from 'immutable';
import update from 'react/lib/update';
import _ from 'underscore';

const 
  defaultFigure = {
    selected: false,
    left: undefined,
    top: undefined
  },
  defaultRect = Object.assign(_(defaultFigure).clone(), {
    fill: '#000000',
    strokeWidth: 1,
    stroke: '#000000',
    width: undefined,
    height: undefined
  }),
  defaultImage = Object.assign(_(defaultFigure).clone(), {
    href: '#',
    width: undefined,
    height: undefined
  }),
  defaultText = Object.assign(_(defaultFigure).clone(), {
    fill: '#000000',
    strokeWidth: 1,
    stroke: '#000000',
    fontSize: 18,
    fontFamily: 'serif',
    content: ''
  });

const FigureRecord = Record(defaultFigure),
  RectRecord = Record(defaultRect),
  ImageRecord = Record(defaultImage),
  TextRecord = Record(defaultText);

let FigureTypeMixin = (superclass, typeName) => class extends superclass {
  serialize() : object {
    return Object.assign(this.toObject(), { type: typeName });
  }
}

export default class Figure extends FigureRecord { };
export const Rect = class extends FigureTypeMixin(RectRecord, 'rectangle') { };
export const Image = class extends FigureTypeMixin(ImageRecord, 'image') { };
export const Text = class extends FigureTypeMixin(TextRecord, 'text') { };