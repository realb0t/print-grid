/*
 * @flow
 */

'use strict';

import type {Action} from './actions';
import {Dispatcher} from 'flux';

const instance: Dispatcher<Action> = new Dispatcher();
export default instance;
export const dispatch = instance.dispatch.bind(instance);
