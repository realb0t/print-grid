/*
 * @flow
 */

'use strict';

const FIGURE_ADD = 'figure/add';
const FIGURE_MOVE = 'figure/move';
const FIGURE_SELECT = 'figure/select';
const FIGURE_ROTATE = 'figure/rotate';
const FIGURE_SCALE = 'figure/scale';
const FIGURE_DELETE = 'figure/delete';
const FIGURE_CHANGE = 'figure/change';

export default {
  FIGURE_ADD: FIGURE_ADD,
  FIGURE_MOVE: FIGURE_MOVE,
  FIGURE_SELECT: FIGURE_SELECT,
  FIGURE_ROTATE: FIGURE_ROTATE,
  FIGURE_SCALE: FIGURE_SCALE,
  FIGURE_DELETE: FIGURE_DELETE,
  FIGURE_CHANGE: FIGURE_CHANGE
}

export type Action =
  {
    type: FIGURE_ADD,
    figureType: string,
    params: object
  } |
  {
    type: FIGURE_SELECT,
    el: object,
    id: number
  } |
  {
    type: FIGURE_MOVE,
    left: number,
    top: number,
    id: number
  } |
  {
    type: FIGURE_ROTATE,
    rotation: number,
    id: number
  } |
  {
    type: FIGURE_SCALE,
    id: number,
    scale: number
  } |
  {
    type: FIGURE_DELETE,
    id: number
  } |
  {
    type: FIGURE_CHANGE,
    id: number,
    params: object
  };

