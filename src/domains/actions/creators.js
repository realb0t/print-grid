/*
 * @flow
 */

'use strict';

import Dispatcher from '../dispatcher';
import ACTIONS from './index';

export function FigureMove(id : number, top : number, left : number) : void {
  Dispatcher.dispatch({
    actionType: ACTIONS.FIGURE_MOVE,
    id: id, left: left, top: top
  });
}

export function FigureSelect(id : number, el : object) : void {
  Dispatcher.dispatch({
    actionType: ACTIONS.FIGURE_SELECT,
    id: id, el: el
  });
}