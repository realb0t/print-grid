import React from "react";
import InlineCss from "react-inline-css";
import Transmit from "react-transmit";
import { Link } from "react-router";
import favicon from "favicon.ico";


/**
 * Main React application entry-point for both the server and client.
 */
class Main extends React.Component {
  /**
   * componentWillMount() runs on server and client.
   */
  componentWillMount () {
    if (__SERVER__) {
      console.log("Hello server");
    }

    if (__CLIENT__) {
      console.log("Hello client");
    }
  }

  /**
   * Runs on server and client.
   */
  render () {
    return (
      <InlineCss stylesheet={Main.css()} namespace="Main">
        <h1>
					Print-Grid
        </h1>
        <ul className="menu-wrapper">
          <li><Link to="/">Main</Link></li>
          <li><Link to="/editor">Editor</Link></li>
          <li><Link to="/help">Help page</Link></li>
        </ul>
        <div className="page-content">
        	{this.props.children}
        </div>
      </InlineCss>
    );
  }
  static css () {
    return (`
      & .github {
        position: absolute;
        top: 0;
        right: 0;
        border: 0;
      }
      & {
        font-family: sans-serif;
        color: #0df;
        padding: 10px 30px 30px;
        max-width: 800px;
        margin: 0px auto;
        background: #222;
      }
      & ul.menu-wrapper {
        list-style-type: none;
        margin: 0 0 1em 0;
        padding: 0;
        overflow: hidden;
        width: 100%;
        background-color: #333; 
      }
      & .menu-wrapper a {
        color: #0df;
      }
      & .menu-wrapper li {
        display: inline-block;
        padding: .5em 1em;
        background-color: #333; 
      }
      & .menu-wrapper li:hover {
        background-color: #555;
      }
    `);
  }
}

/**
 * Use Transmit to query and return GitHub stargazers as a Promise.
 */
export default Main;

