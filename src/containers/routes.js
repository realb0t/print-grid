import React from "react";
import {Router, Route, IndexRoute, browserHistory} from "react-router";

import Main from "./Main";
import EditorPage from "../components/EditorPage";
import HelpPage from "../components/HelpPage";
import IndexPage from "../components/IndexPage";

/**
 * The React Router routes for both the server and the client.
 */
module.exports = (
	<Router history={browserHistory}>
		<Route path="/" component={Main}>
      <IndexRoute component={IndexPage} />
      <Route path="editor" component={EditorPage} />
      <Route path="help" component={HelpPage} />
    </Route>
	</Router>
);
