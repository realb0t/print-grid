import React, { Component } from 'react';
import InlineCss from "react-inline-css";
import Exporter from "./Editor/Exporter";
import Canvas from "./Editor/Canvas";

export default class Editor extends Component {

  static defaultProps = { 
    // http://www.a4papersize.org/a4-paper-size-in-pixels.php
    width: 773 / 2,
    height: 1095 / 4,
    widthPoints: 'px',
    heightPoints: 'px'
  };
  static propTypes = { 
    width: React.PropTypes.number, 
    height: React.PropTypes.number, 
    widthPoints: React.PropTypes.oneOf(['px', 'em', '%']),
    heightPoints: React.PropTypes.oneOf(['px', 'em', '%'])
  };

  css (params) {
    return (`
      @font-face {
        font-family: 'Lora';
        font-style: normal;
        font-weight: 400;
        src: url(/fonts/Lora.ttf) format('truetype');
      }

      @font-face {
        font-family: 'Marmelad';
        font-style: normal;
        font-weight: 400;
        src: url(/fonts/Marmelad.ttf) format('truetype');
      }

      & .editor-actions-btns button {
        padding: .7em;
        cursor: pointer;
        margin: 1em .5em 1em 0;
        font-size: 1em;
        background-color: #1DABC3;
        border: none;
        color: white;
      }
      & .editor-wrapper {
        background-color: #E6E6E6;
        width: 100%;
      }
    `);
  }
  constructor(props) {
    super(props);
    this.state = {
      printing: false
    }
  }
  handleSave () {
    alert('Save: ' + JSON.stringify(this.elements));
  }
  elements = [
    { type: 'image', top: 20, left: 80, 
      width: (646 / 4), height: (541 / 4),
      href: 'http://localhost:8000/images/apricot.jpg' },
    { type: 'text', top: 80, left: 80, content: '{{name}}' }
  ]
  handleUpdateElements(elements) {
    this.elements = elements;
    return;
  }
  handlePrint (e) {
    if (this.state.printing) { return; }

    this.setState({ printing: true })
    
    const svg = this.refs.svgContainer.innerHTML,
      data = { width: this.props.width, height: this.props.height },
      metaData = this.props.format.getData();
    
    Exporter.exportPDF(svg, data, metaData).then((response) => {
      var a = document.createElement("a");
      a.href = window.URL.createObjectURL(response);
      a.download = response.name || "for_print";
      a.click();
      //window.onfocus = function () {  
      //  document.body.removeChild(a)
      //}
      this.setState({ printing: false });
    }).catch((err) => {
      alert(err.message);
      this.setState({ printing: false });
    });
  }

  render() {
    
    const printBtnLabel = (this.state.printing) ? 'Printing ...' : 'Print';

    return (
      <InlineCss stylesheet={this.css(this.props)} namespace="editor">
        <div className="editor-wrapper">
          <div className="editor-canvas" ref="svgContainer">
            <Canvas {...this.props} elements={this.elements} handleUpdateElements={this.handleUpdateElements.bind(this)} />
          </div>
        </div>
        <div className="editor-actions-btns">
          <button className="action-btn action-btn_save" onClick={this.handleSave.bind(this)}>Save</button>
          <button className="action-btn action-btn_print" onClick={this.handlePrint.bind(this)}>{printBtnLabel}</button>
        </div>
      </InlineCss>
    );
  }
};