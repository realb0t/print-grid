import React, { Component, PropTypes } from 'react';
import InlineCss from "react-inline-css";
import _ from 'underscore';
import StateMachine from "javascript-state-machine";

class WizardComponent extends Component {
  static propTypes = {
    initialize: PropTypes.string.isRequired,
    transitions: PropTypes.array.isRequired
  }
  constructor(props) {
    super(props);
    this.fsm = StateMachine.create({
      initial: this.props.initialize,
      events: this.props.transitions
    });
    this.state = { step: this.fsm.current };
  }
  css (params) {
    return (`
      & ul.wizard-nav {
        list-style-type: none;
        margin: 0;
        padding: 0;
        overflow: hidden;
        width: 100%;  
      }

      & .wizard-nav li {
        display: inline-block;
        padding: .5em 1em .5em 0;
      }

      & .wizard-nav a {
        color: white;
      }
    `);
  }
  tryFire(events, clickEvent) {
    events.find(function(event) {
      if (this.fsm.can(event)) {
        this.fsm[event]();
        return true;
      }
    }.bind(this));
    clickEvent.preventDefault();
    return this.setState({ step: this.fsm.current });
  }
  render() {
    const content = this.props.children[this.state.step];
    return (
      <InlineCss stylesheet={this.css(this.props)} namespace="wizard">
        <div className="wizard-container">
          <ul className="wizard-nav">
            <li><a href="#" onClick={this.tryFire.bind(this, [ 'prevFormat' ])}>Print format</a></li>
            <li><a href="#" onClick={this.tryFire.bind(this, [ 'nextEdit' ])}>Editor</a></li>
          </ul>
          <div className="wizard-step">
            {content}
          </div>
          <button className="wizard-next-ste-btn" onClick={this.tryFire.bind(this, [ 'nextEdit' ])}>Next</button>
        </div>
      </InlineCss>
    );
  }
}

function WizardStep() {
  return function(...args) {
    return {...args};
  }
}

export default WizardComponent;