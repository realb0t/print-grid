"use strict";

import React, { Component, PropTypes } from 'react';
import InlineCss from "react-inline-css";

export default class PrintFormat extends Component {
  css (params) {
    return ('');
  }
  constructor(props){
    super(props);
    this.state = this.props.format.getData();
  }
  handleFormatChange(e) {
    this.setState({ format: e.target.value });
  }
  componentDidUpdate(prevProps, prevState){
    this.props.changeStep(this.state);
  }
  render(){
    return (
      <InlineCss stylesheet={this.css(this.props)} namespace="print-format">
        <div className="print-format-wrapper">
          Choose Print format

          <div className="print-format-param print-format-param_paper">
            <label>Paper format:</label>
            <select name="papper-format" defaultValue={this.state.format} onChange={this.handleFormatChange.bind(this)}>
              <option value="A4">A4</option>
              <option value="letter">Letter</option>
            </select>
          </div>

          <div className="print-format-param print-format-param_orientation">
            <label>Orientation:</label>
            <select name="orientation" defaultValue={this.state.orientation}>
              <option value="portrait">Portrait</option>
              <option value="lanscape">Lanscape</option>
            </select>
          </div>

          <div className="print-format-param print-format-param_grid">
            <label>Orientation:</label>
            <select name="grid" defaultValue={this.state.grid}>
              <option value="2x3">2x3</option>
              <option value="2x2">2x2</option>
            </select>
          </div>

        </div>
      </InlineCss>
    );
  }
}