import React, { Component, PropTypes } from 'react';
import update from 'react/lib/update';
import ItemTypes from './Figure/ItemTypes';
import { DropTarget, DragDropContext } from 'react-dnd';
import MouseBackend from 'vendor/dnd-mouse-backend';
import _ from 'underscore';
import ElementFactory from './Figure/Factory';
import { Container } from 'flux/utils';
import { FigureMove, FigureSelect } from "../../domains/actions/creators"
import FigureStore from "../../domains/figure/store"

const moveAction = function(props, monitor, component) {
  const item = monitor.getItem();
  const delta = monitor.getDifferenceFromInitialOffset();
  const left = Math.round(item.left + delta.x);
  const top = Math.round(item.top + delta.y);
  const entity = monitor.getItemType();
  
  FigureMove(item.id, top, left);
}

const figureTarget = {
  drop(props, monitor, component) {
    moveAction(props, monitor, component);
  },
  hover(props, monitor, component) {
    moveAction(props, monitor, component);
  },
  canDrop(props, monitor) {
    return true;
  }
};

class Canvas extends Component {
  static propTypes = {
    connectDropTarget: PropTypes.func.isRequired
  }

  static getStores() {
    return [FigureStore];
  }

  static calculateState(prevState) {
    return {
      elements: FigureStore.getState().map(fig => {
        return fig.serialize()
      } ).toArray()
    };
  }

  handleSelect(el, index, e) {
    FigureSelect(index, el);
  }

  render() {
    const { connectDropTarget } = this.props;
    const figures = this.state.elements.map((el, index) => {
        let props = _(el).extend({ 
          onSelect: this.handleSelect.bind(this, el, index) 
        });
        return ElementFactory[el.type](index, props);
      });

    return connectDropTarget(
      <svg style={{ backgroundColor: 'white', position: 'relative' }}
        width={this.props.width} height={this.props.height}>
        {figures}
      </svg>
    );
  }
}

export default DragDropContext(MouseBackend)(
  DropTarget(_(ItemTypes).values(), figureTarget, connect => ({
      connectDropTarget: connect.dropTarget()
    }))(Container.create(Canvas))
);