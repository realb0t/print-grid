// https://gist.github.com/olmokramer/82ccce673f86db7cda5e
const colorRegex = /(#(?:[\da-f]{3}){1,2}|rgb\((?:\d{1,3},\s*){2}\d{1,3}\)|rgba\((?:\d{1,3},\s*){3}\d*\.?\d+\)|hsl\(\d{1,3}(?:,\s*\d{1,3}%){2}\)|hsla\(\d{1,3}(?:,\s*\d{1,3}%){2},\s*\d*\.?\d+\))/;

export default {
  colorValidator: function(props, propName, componentName) {
    let value = props[propName];
    if (!colorRegex.test(value.toLowerCase())) {
      return new Error(`Props color ${propName} = ${value} is not color definition`);
    }
  }
};