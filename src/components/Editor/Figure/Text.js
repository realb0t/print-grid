import React, { Component, PropTypes } from 'react';
import Tools from "./Tools";
import _ from 'underscore';
import ElementSource from './Source';

const inlineStyles = {
  WebkitTouchCallout: 'none',
  WebkitUserSelect: 'none',
  KhtmlUserSelect: 'none',
  MozUserSelect: 'none',
  MsUserSelect: 'none',
  userSelect: 'none',  
  cursor: 'move'
};

@ElementSource()
export default class Text extends Component {
  static defaultProps = { 
    top: 80,
    left: 60,
    fill: '#000000',
    fontSize: 18,
    fontFamily: 'serif'
  }
  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    id: PropTypes.any.isRequired,
    left: PropTypes.number.isRequired, 
    top: PropTypes.number.isRequired,
    fill: Tools.colorValidator,
    fontSize: PropTypes.number.isRequired,
    fontFamily: PropTypes.string
  }
  render () {
    const { left, top, connectDragSource, isDragging, fill, onSelect } = this.props;
    const fontStyles = _(this.props)
      .chain()
      .pick('fontSize', 'fontFamily')
      .extend(inlineStyles)
      .value();

    return connectDragSource(
      <text style={fontStyles}
        x={left} y={top} onClick={onSelect}
        fill={this.props.fill}>
        {this.props.children}
      </text>
    );
  }
}