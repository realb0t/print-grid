import { DragSource, DragDropContext } from 'react-dnd';
import ItemTypes from './ItemTypes';

const boxSource = {
  beginDrag(props) {
    const { id, left, top } = props;
    return { id, left, top };
  },
  canDrag() {
    return true;
  }
};

export default function() { 
  return function(...args) {
    return DragSource(ItemTypes.ELEMENT, boxSource, (connect, monitor) => ({
      connectDragSource: connect.dragSource(),
      isDragging: monitor.isDragging()
    }))(...args);
  }
}