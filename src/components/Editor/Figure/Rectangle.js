import React, { Component } from 'react';
import Tools from "./Tools";
import _ from 'underscore';

export default class extends Component {
  static defaultProps = { 
    width: 150, 
    height: 150,
    y: 50,
    x: 50,
    fill: '#66FFFF',
    strokeWidth: 1,
    stroke: '#FFFFFF'
  }
  static propTypes = {
    width: React.PropTypes.number.isRequired, 
    height: React.PropTypes.number.isRequired,
    x: React.PropTypes.number.isRequired, 
    y: React.PropTypes.number.isRequired,
    fill: Tools.colorValidator,
    stroke: Tools.colorValidator,
    strokeWidth: React.PropTypes.number
  }
  constructor(props, context) {
    super(props, context);
    this.state = _(props).pick('width', 'height', 'y', 'x', 
      'fill', 'strokeWidth', 'stroke');
  }
  render () {
    return (
      <rect {...this.state} />
    );
  }
}