import React, { Component, PropTypes } from 'react';
import ElementSource from './Source';

@ElementSource()
export default class Image extends Component {
  static defaultProps = { 
    href: 'http://localhost:8000/images/jazz-bass.jpg',
    width: 200,
    height: 3 / 4 * 200
  }
  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    id: PropTypes.any.isRequired,
    left: PropTypes.number.isRequired, 
    top: PropTypes.number.isRequired,

    href: PropTypes.string.isRequired,
    width: PropTypes.number.isRequired,
    height: PropTypes.number.isRequired,
  }
  render () {
    const { 
      hideSourceOnDrag, 
      width, height, href,
      left, top, connectDragSource, 
      isDragging, onSelect
    } = this.props;

    return connectDragSource(
      <image width={width} height={height}
        onClick={onSelect}
        xlinkHref={href} fill={'black'}
        x={left} y={top} style={{ cursor: 'move', position: 'absolute' }} />
    );
  }
}