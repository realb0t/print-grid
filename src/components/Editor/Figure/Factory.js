import React, { Component, PropTypes } from 'react';
import Image from "./Image";
import Rectangle from "./Rectangle";
import Text from "./Text";
import _ from "underscore";

export default {
  image(index, props) { 
    return <Image {...props} id={index} key={'element_image_' + index} /> 
  },
  rectangle(index, props) {
    return <Rectangle {...props} id={index} key={'element_rectangle_' + index} />
  },
  text(index, props) {
    const content = props.content;
    const elProps = props
    return <Text {...elProps} id={index} key={'element_text_' + index}>{content}</Text>
  }
};