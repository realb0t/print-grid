'use strict';

const Exporter = {
  exportPDF: function(svg, data, metaData) {
    return new Promise((resolve, reject) => {
      var request = new XMLHttpRequest(),
      requestData = { 
        svg: svg, 
        data: data,
        meta: metaData
      };

      request.open("POST", "/print", true);
      request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
      request.responseType = "blob";
      request.onload = function (e) {
        if (this.status === 200) {
          resolve(this.response);
        } else {
          throw new Error('Export PDF error: ' + this.status); 
        }
      };
      request.send(JSON.stringify(requestData));
    });
  }
}

export default Exporter;