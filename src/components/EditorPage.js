import React, { Component } from 'react';
import InlineCss from "react-inline-css";
import WizardComponent from "./EditorPage/Wizard";
import FormatModel from "./../domains/format/model";
import PrintFormat from "./EditorPage/PrintFormat";
import Templates from "./EditorPage/Templates";
import Editor from "./Editor";

const wizardTransition = [
  { name: 'nextEdit', from: 'format', to: 'editor' },
  { name: 'prevFormat', from: 'editor', to: 'format' },
]

const format = new FormatModel();

export default class extends Component {
  static defaultProps = {
    format: format
  }
  css() {
    return (`
    `);
  }
  constructor(props) {
    super(props);
    this.state = {};
  }
  changePageFormat(data) {
    this.props.format.setData(data);
  }
  render() {
    return (
      <InlineCss stylesheet={this.css(this.props)} namespace="editor-page">
      <div className="editor-page">
        <WizardComponent initialize="format" transitions={wizardTransition}>
          {{ 
            format: <PrintFormat format={this.props.format} changeStep={this.changePageFormat.bind(this)} />,
            editor: <Editor format={this.props.format} /> 
          }}
        </WizardComponent>
      </div>
      </InlineCss>
    );
  }
};